/**
 * Copyright 2019 cpke
 */

#include <iostream>
#include "include/GPIO.h"
#include <unistd.h>

int main() {
    /* Create a GPIO object for GPIO18
     * sysfs uses Broadcom SOC channel (BCM) for its numbering
     */
    GPIO my_led = GPIO(18);
    try {
        my_led.set_direction(GPIO_OUTPUT); // output pin mode
        my_led.set_value(1); // turn on LED
        usleep(10000000);   // sleep 10 seconds (parameter is nanoseconds)
        my_led.set_value(0); // turn off LED
    } catch (const std::exception &e) {
        perror(e.what());
        return 1;
    } catch (...) {
        perror("An unexpected error has occurred");
        return 1;
    }


    return 0;
}
