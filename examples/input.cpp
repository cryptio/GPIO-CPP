/**
 * Copyright 2019 cpke
 */

#include <iostream>
#include "include/GPIO.h"
#include <unistd.h>

int main() {
    // Create a GPIO object for GPIO18
    // sysfs uses Broadcom SOC channel (BCM) for its numbering
    GPIO my_led = GPIO(18);
    try {
        int input_state = 0;
        my_led.set_direction(GPIO_INPUT);
        for (;;) {
            usleep(500000);
            input_state = my_led.get_value();
            if (inputState == GPIO_HIGH) {
                std::cout << "Received high signal\n";
            }
        }
    } catch (const std::exception &e) {
        perror(e.what());
        return 1;
    } catch (...) {
        perror("An unexpected error has occurred");
        return 1;
    }

    return 0;
}
