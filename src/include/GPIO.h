/**
 * Copyright 2019 cpke
 */

#ifndef GPIO_GPIO_H
#define GPIO_GPIO_H

#include <string>

#define GPIO_OUTPUT "out"
#define GPIO_INPUT "in"
#define GPIO_HIGH 1
#define GPIO_LOW 0


// A GPIO object represents a GPIO pin
class GPIO {
public:
    GPIO() = delete;    // Default constructor is explicitly disabled

    explicit GPIO(const unsigned &channel);

    /**
     * Set the GPIO channel direction
     * @param direction The direction to set, which should be set using the GPIO macros
     */
    void set_direction(const std::string &direction) const;

    /**
     * Set the GPIO pin value
     * @param value The value to set, which should be either GPIO_HIGH or GPIO_LOW
     */
    void set_value(const unsigned &value) const;

    /**
     * Get the current GPIO pin value
     * @return The current value
     */
    int get_value() const;

    /**
     * Get the current GPIO pin direction
     * @return The current pin direction
     */
    std::string get_direction() const;

    /**
     * Get the current GPIO pin number
     * @return The current GPIO pin number
     */
    const unsigned &get_gpio_num() const;

    ~GPIO();

private:
    // Export and unexport should not be manually called
    void export_gpio() const;

    void unexport_gpio() const;

    unsigned gpio_num_;

};

#endif //GPIO_GPIO_H
