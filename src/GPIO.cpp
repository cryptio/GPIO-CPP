/**
 * Copyright 2019 cpke
 */

#include "gpio.h"
#include <fstream>
#include <iostream>

GPIO::GPIO(const unsigned &chan) : GPIO_num_(chan) {
    // Automatically export the GPIO pin on instantiation
    export_GPIO();
}

void GPIO::export_GPIO() const {
    const std::string export_path = "/sys/class/gpio/export";
    std::ofstream os(export_path.c_str());  // open export file
    if (!os) // Error opening file
        throw std::runtime_error("Unable to export: " + strerror(errno));
    os << GPIO_num_;    // write GPIO number to export
    os.close();
}

void GPIO::unexport_GPIO() const {   // tells Linux that you're not using the pin anymore
    const std::string uexport_path = "/sys/class/gpio/unexport";
    std::ofstream os(unexport_path.c_str());  // open unexport file
    if (!os) // Error opening file
        throw std::runtime_error("Unable to unexport: " + strerror(errno));
    os << GPIO_num_;    // write GPIO number to unexport
    os.close();
}

void GPIO::set_direction(const std::string &direction) const {
    const bool valid_direction = ((direction == GPIO_INPUT) || (direction == GPIO_OUTPUT));
    if (!valid_direction) {
        throw std::invalid_argument("Invalid direction");
    }

    const std::string set_direction_path = "/sys/class/gpio/gpio" + std::to_string(GPIO_num_) + "/direction";
    std::ofstream os(set_direction_path.c_str());
    if (!os) // Error opening file
        throw std::runtime_error("Unable to set direction: " + strerror(errno));
    os << direction;    // write direction
    os.close();
}

void GPIO::set_value(const unsigned &value) const {
    const std::string get_value_path = "/sys/class/gpio/gpio" + std::to_string(GPIO_num_) + "/value";
    std::ofstream os(get_value_path.c_str());
    if (!os)    // Error opening file
        throw std::runtime_error("Unable to set value: " + strerror(errno));
    os << value;    // write value
    os.close();
}

int GPIO::get_value() const {
    const std::string get_value_path = "/sys/class/gpio/gpio" + std::to_string(GPIO_num_) + "/value";
    std::ifstream is(get_value_path.c_str());
    if (!is)    // Error opening file
        throw std::runtime_error("Unable to get value: " + strerror(errno));
    int val;
    is >> val;
    bool valid_val = ((val == GPIO_HIGH) || (val == GPIO_LOW));
    if (!valid_val)
        throw std::runtime_error("Read invalid value");
    is.close();

    return val;
}

std::string GPIO::get_direction() const {
    const std::string get_direction_path = "/sys/class/gpio/gpio" + std::to_string(GPIO_num_) + "/direction";
    std::ifstream is(get_direction_path.c_str());
    if (!is)    // Error opening file
        throw std::runtime_error("Unable to get direction: " + strerror(errno));
    std::string direction;
    is >> direction;

    return direction;

}

const unsigned &GPIO::get_GPIO_num() const {
    return GPIO::GPIO_num_;
}

GPIO::~GPIO() {
    // Automatically unexport the GPIO pin on destruction
    unexport_GPIO();
}