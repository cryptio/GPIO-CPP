# GPIOCPP
A C++ class for controlling GPIO pins,  inspired by the following [tutorial](http://www.hertaville.com/introduction-to-accessing-the-raspberry-pis-gpio-in-c.html).

## Usage
To use this class, instantiate a GPIO object with the desired BCM pin number.

```cpp
GPIO my_led = GPIO(18);
```

Next, set your desired direction.

```cpp
my_led.set_direction(GPIO_INPUT);
```

From here, you can use the library however you want. Refer to [examples](examples) to see how you can turn on GPIO pins or read their input.

